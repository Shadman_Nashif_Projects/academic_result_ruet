package com.example.rejuthebest.academicresultsruet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class Profile extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private List<Address> addressEntityList = new ArrayList<Address>();
    private List<City> cityEntityList = new ArrayList<City>();
    private ListView listView;
    private AddressAdapter adapter;
    private CityAdapter cityAdapter;
    private Spinner citySpinner;
    TextView tv, dept, roll, backlog;
    TextView name;

    Address[] address;
    City[] city;

    public static final String LOGINPREF = "loginpref";
    private SharedPreferences sharedPreferences;
    //private SharedPreferences.Editor editor;
    Context context;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        context = this;
        // make a sharedpreference
        sharedPreferences = context.getSharedPreferences(LOGINPREF, Context.MODE_PRIVATE);
        //editor = sharedPreferences.edit();

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            username = extras.getString("USERNAME");
        }

        citySpinner = (Spinner) findViewById(R.id.citySpinner);
        listView = (ListView) findViewById(R.id.address_listview);
        tv = (TextView) findViewById(R.id.textView8);
        name = (TextView) findViewById(R.id.textView9);
        dept = (TextView) findViewById(R.id.textView10);
        roll = (TextView) findViewById(R.id.textView12);
        backlog = (TextView) findViewById(R.id.textView18);

        // now call the server class
        new uploadToServer().execute();

        cityAdapter = new CityAdapter(this, android.R.layout.simple_spinner_dropdown_item, loadDummyCities());
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setAdapter(cityAdapter);
        citySpinner.setOnItemSelectedListener(this);
        loadDummyAddress();
        adapter = new AddressAdapter(this, addressEntityList);
        listView.setAdapter(adapter);

        TextView tv = (TextView) findViewById(R.id.textView12);
        tv.setText(username);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        //loadDummyAddress();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            return true;
        }
        if (id == R.id.action_exit) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private List<City> loadDummyCities() {
        cityEntityList = new ArrayList<City>();

        // set all of those spinner item
        city = new City[8];

        for(int i = 0; i < 8; i++){
            city[i] = new City();
            city[i].setId(i+1);
            city[i].setCity(getPostfixNumberYear(i+1) + " " + getPostfixNumberSemester(i+1));
            cityEntityList.add(city[i]);
        }

        return cityEntityList;
    }

    // this function is for getting the postfix for the year
    private String getPostfixNumberYear(int num){
        switch (num){
            case 1:
            case 2:
                return "1st Year";
            case 3:
            case 4:
                return "2nd Year";
            case 5:
            case 6:
                return "3rd Year";
            case 7:
            case 8:
                return "4rth Year";
        }
        return "";
    }

    // this function is for getting the postfix for the semester
    private String getPostfixNumberSemester(int num){
        switch (num){
            case 1:
                return "1st semester";
            case 2:
                return "2nd semester";
            case 3:
                return "3rd semester";
            case 4:
                return "4rth semester";
            case 5:
            case 6:
            case 7:
            case 8:
                return num+"th semester";
        }
        return "";
    }

    private List<Address> loadDummyAddress() {
        tv.setText("Welcome");

        addressEntityList = new ArrayList<Address>();

        // set the fields of those addresses for each of those spinner item
        address = new Address[10];
        for(int i = 0; i < 9; i++){
            address[i] = new Address();
            addressEntityList.add(address[i]);
        }

        // now get the result for each of those address field
        /*
        for (int i = 8; i >= 1; i--) {
            new uploadToServerList().execute(city[i].getId()+"");
        }
        */
        for (int i = 0; i < 8; i++) {
            new uploadToServerList().execute(city[i].getId()+"");
        }

        return addressEntityList;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        City city = cityAdapter.getItem(position);

        //Here we use the Filtering Feature which we implemented in our Adapter class.
        adapter.getFilter().filter(Long.toString(city.getId()), new Filter.FilterListener() {
            @Override
            public void onFilterComplete(int count) {

            }
        });
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    // this section for the upper information section in the layout for basic profile information
    private class uploadToServer extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        public uploadToServer() {
        }

        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true);
        }

        @Override
        protected String doInBackground(String... arg) {
            try {
                String link = "http://engineerejucse2012.net23.net/info.php";
                String data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write(data);
                wr.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                return sb.toString();

            } catch (Exception e) {
                //Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                return new String("Error: " + e.getMessage());
            }
        }

        protected void onPostExecute(String result) {
            Log.d("Details", result);

            //Toast.makeText(context, result, Toast.LENGTH_LONG).show();
            if (!result.equals("")) {
                String[] value = result.split("//");

                // set the text of those corresponding textviews
                name.setText(value[0]);
                dept.setText(value[1]);
                roll.setText(value[2]);

                // get the backlog of this username
                String backlogText = "";
                for (int i = 3; i < value.length; i++) {
                    backlogText += value[i] + ", ";
                }

                // now finally set the text in the backlog textview
                backlog.setText(backlogText);
            }

            progressDialog.dismiss();
        }
    }

    // this section for the list view section
    private class uploadToServerList extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String semester;

        public uploadToServerList() {
        }

        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true);
        }

        @Override
        protected String doInBackground(String... arg) {
            try {
                semester = arg[0];
                String link = "http://engineerejucse2012.net23.net//listview.php";
                String data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");
                data += "&" + URLEncoder.encode("semester", "UTF-8") + "=" + URLEncoder.encode(semester, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write(data);
                wr.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                return sb.toString();

            } catch (Exception e) {
                Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                return new String("Error: " + e.getMessage());
            }
        }

        protected void onPostExecute(String result) {
            Log.d("DetailsList", result);

            int semesterID = Integer.parseInt(semester);

            if (!result.equals("")) {
                String[] value = result.split("//");
                int step = -1;
                int loop = value.length / 4;

                for(int i = 0; i < loop; i++){
                    address[i].setId(semesterID);
                    address[i].setCityId(semesterID);
                    address[i].setBuildingName(value[++step]);
                    address[i].setStreet(value[++step]);
                    address[i].setGpa(value[++step]);
                    address[i].setArea(value[++step]);
                }
            }

            progressDialog.dismiss();
        }
    }
}
