package com.example.rejuthebest.academicresultsruet;

/**
 * Created by REJU THE BEST on 8/2/2016.
 */
public class Address {
    private long id;
    private long cityId;
    private String buildingName;
    private String street;
    private String area;
    private String gpa;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
    public String getGpa() {
        return gpa;
    }

    public void setGpa(String gpa) {
        this.gpa = gpa;
    }
    public String getArea() {
        return area;
    }
    public void setArea(String area) {
        this.area = area;
    }
}
