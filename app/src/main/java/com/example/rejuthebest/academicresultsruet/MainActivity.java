package com.example.rejuthebest.academicresultsruet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {







    public static final String USER_NAME = "USER_NAME";

    public static final String PASSWORD = "PASSWORD";

    private static final String LOGIN_URL = "http://engineerejucse2012.net23.net/login.php";

    public static final String LOGINPREF = "loginpref";

    private EditText editTextUserName;
    private EditText editTextPassword;

    private Button buttonLogin;
    private Button buttonReg;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        editTextUserName = (EditText) findViewById(R.id.editText);
        editTextPassword = (EditText) findViewById(R.id.editText2);
        buttonLogin = (Button) findViewById(R.id.Lbutton);
        buttonReg = (Button) findViewById(R.id.Rbutton);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        buttonReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallRegister();
            }
        });


        // make a shared preferences
        context = this;
        sharedPreferences = context.getSharedPreferences(LOGINPREF, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void CallRegister() {
        Intent RegUser = new Intent(MainActivity.this,Register.class);
        startActivity(RegUser);
    }

   /*
    public void onButtonClick (View v) {


        Intent intent1    = new Intent(MainActivity.this,Register.class);
        if (v== buttonReg ) {
            //intent.putExtra("Username", str);
            startActivity(intent1);

        }
       /* if (v.getId()== R.id.Lbutton)
        {

            EditText e = (EditText) findViewById(R.id.editText);
            String str = e.getText().toString();
            Intent intent = new Intent(MainActivity.this,Profile.class);
            intent.putExtra("Username", str);
            startActivity(intent);
        }

    }
   */

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            return true;
        }
        if (id == R.id.action_exit) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        android.app.FragmentManager fragmentManager = getFragmentManager();

        if (id == R.id.item1) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame1, new FirstFragment())
                    .commit();

            // Handle the camera action
        } else if (id == R.id.item2) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame1, new SecondFragment())
                    .commit();

        } else if (id == R.id.item3) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame1, new ThirdFragment())
                    .commit();

        } else if (id == R.id.share) {

        } else if (id == R.id.send) {

        } else if (id == R.id.visit) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }






    private void login(){
        String username = editTextUserName.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        // for this time login is not available
        userLogin(username,password);
    }

    private void userLogin(String usernameText, final String password){
        final String username = usernameText;
        class UserLoginClass extends AsyncTask<String,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(MainActivity.this,"Please Wait",null,true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equalsIgnoreCase("success")){
                    Intent intent = new Intent(MainActivity.this,Profile.class);
                    intent.putExtra("USERNAME",username);

                    // save the username in the sharedpreferences
                    editor.putString("USERNAME", username);
                    editor.commit();

                    startActivity(intent);
                }else{
                    Toast.makeText(MainActivity.this,s,Toast.LENGTH_LONG).show();
                }
            }

            @Override
            protected String doInBackground(String... params) {
                HashMap<String,String> data = new HashMap<>();
                data.put("username",params[0]);
                data.put("password",params[1]);

                RegisterUserClass ruc = new RegisterUserClass();

                String result = ruc.sendPostRequest(LOGIN_URL,data);

                return result;
            }
        }
        UserLoginClass ulc = new UserLoginClass();
        ulc.execute(username,password);
    }



  /*  public void swtreg1(View v)
    {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
  */

    }
