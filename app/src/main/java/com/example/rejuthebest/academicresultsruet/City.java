package com.example.rejuthebest.academicresultsruet;

/**
 * Created by REJU THE BEST on 6/29/2016.
 */
public class City {
    private long id;
    private String city;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {

        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
